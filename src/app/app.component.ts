import {Component} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {PDFDocument} from 'pdf-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ImageOnPdf';

  pdfFile!: File;
  imageFile!: File;

  constructor(private sanitizer: DomSanitizer) {
  }

  getPdf($event: any) {
    this.pdfFile = $event.target.files[0];
  }

  getImage($event: any) {
    this.imageFile = $event.target.files[0];
  }

  async download() {
    this.downloadUint8ArrayAsPdf(await this.addSignatureToPdf(await this.pdfFile.arrayBuffer(), await this.imageFile.arrayBuffer()), 'ranasing')
  }

  addSignatureToPdf(pdfBytes: ArrayBuffer, signatureImage: ArrayBuffer) {
    return PDFDocument.load(pdfBytes)
      .then(async (pdfDoc) => {
        const image = await pdfDoc.embedPng(signatureImage);

        const pages = pdfDoc.getPages();
        const firstPage = pages[0];
        const {width, height} = firstPage.getSize();

        firstPage.drawImage(image, {
          x: 50,
          y: 50,
          width: 100,
          height: 50,
          opacity: 0.5,
        });

        return pdfDoc.save();
      });
  }

  downloadUint8ArrayAsPdf(uint8Array: any, fileName: any) {
    const blob = new Blob([uint8Array], {type: 'application/pdf'});
    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = url;
    a.download = fileName || 'document.pdf';

    // Append the anchor to the body
    document.body.appendChild(a);

    // Trigger a click on the anchor
    a.click();

    // Remove the anchor from the body
    document.body.removeChild(a);

    // Revoke the URL to free up resources
    URL.revokeObjectURL(url);
  }


}
